const {yellow, blue, red} = require('chalk');

const balls = (width) => {
    const ball = 'o';
    let str = '';
    
    while (width > 0) {
        const rand = Math.floor(Math.random() * Math.floor(3));
        str += rand % 2 ? '' : ' ';
        switch (rand) {
        case 0:
            str += yellow(ball);
            break;
        case 1:
            str += blue(ball);
            break;
        case 2:
            str += red(ball);
            break;
        case 3:
        case 4:
            str += ' ';
            break;
        default:
            break;
        }
        str += rand % 2 ? ' ' : '';
        width -= 2;
    }

    return str;
};

module.exports = balls;
