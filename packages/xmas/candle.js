const {yellowBright} = require('chalk');

const candle = () => yellowBright('i');

module.exports = candle;