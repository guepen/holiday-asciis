const balls = require('./balls');
const candle = require('./candle');
const star = require('./star');
const tree = require('./tree');
const text = require('./text');

module.exports = {
    balls,
    candle,
    star,
    tree,
    text
};
