const {yellow} = require('chalk');

const star = (padStartStr = '') => {
    const spaces = 2;
    let str = `${padStartStr + ' '.repeat(spaces)}/\\`;
    str += '\n';
    str += `${padStartStr}<${' '.repeat(spaces * 2)}>`;
    str += '\n';
    str += `${padStartStr + ' '.repeat(spaces)}\\/`;
    
    return yellow(str);
};

module.exports = star;