const {
    textSync: figSync, 
    text: figText
} = require('figlet');
const chalk = require('chalk');

const textSync = (color, text) => chalk[color](figSync(text));

const text = (color, text) => chalk[color](figText(text));

module.exports = {
    sync: textSync,
    async: text
};
