const {
    green, 
    blackBright,
    cyanBright
} = require('chalk');

const candle = require('./candle');
const star = require('./star');
const balls = require('./balls');

const tree = () => {
    const leftTwig = green('/');
    const rightTwig = green('\\');

    const height = 11;
    const width = height * 2 - 1;
    let spaces = parseInt(width / 2);
    let counter = 0;
    let treeStr = '';

    treeStr += star(' '.repeat(spaces - 2));
    treeStr += '\n';

    while (spaces >= 0) {
        let str = '';

        str += ' '.repeat(spaces);

        if (spaces > 0) {
            if (counter % 2) {
                str += `${candle() + leftTwig}${cyanBright('~').repeat(counter * 2)}${rightTwig + candle()}`;
                str = str.substring(1);
            } else {
                const content = balls((counter * 2));
                str += `${leftTwig}${content}${rightTwig}`;
            }
        } else { // last line 
            str += `${' '.repeat(counter - 1)}${blackBright('|__|')}`;
        }
        
        treeStr += str;
        treeStr += '\n';

        counter++;
        spaces--;
    }

    return treeStr;
};

module.exports = tree;